<?php
//Получаем параметр из коммандной строки
if(!empty($argv) && count($argv) < 2){exit('Не заданы параметры! Введите страну.');}

if (count($argv) >= 1)
    {
        //Значит страна состоит из 2 и более слов
        $separated_stroka = implode(' ',array_slice($argv,1));
    }else
        {$separated_stroka =$argv[1];}

//Запрашиваем данные с удаленного ресурса
$datacsv = file_get_contents('https://raw.githubusercontent.com/netology-code/php-2-homeworks/master/files/countries/opendata.csv');
//Записываем в файл (если файла нет, то он создастся по указанному пути)
$file = file_put_contents('./site.csv',$datacsv);
//Обрабатываем сохраненный файл
$row = 1;
if (($handle = fopen("./site.csv", "r")) !== FALSE) {

    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        //Находим строку с переданным параметром и выводи данные о визовом режиме
        if ($data[1] === $separated_stroka)
            {
                echo ('Страна: '.$separated_stroka.' - Режим въезда:'.$data[4]);
            }
            $row++;}
    }
    //Закрываем файл
    fclose($handle);


