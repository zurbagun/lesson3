<?php
$Params = $argv;
if(count($Params) < 2)
{
 echo 'Не введены параметры для поиска';
 exit();
}
//Обрабатываем переданные параметры из командной строки
$ArrayBooks = array_slice($Params, 1);
$NameBooks = implode(' ',$ArrayBooks);
//Кодируем строку заапроса
$StrQuery = urlencode(trim($NameBooks));
//Запрашиваем данные с удаленного ресурса
$dataJson = file_get_contents('https://www.googleapis.com/books/v1/volumes?q='.$StrQuery);
//Записываем в файл (если файла нет, то он создастся по указанному пути) ДЛЯ СЕБЯ
$file = file_put_contents('./books.json',$dataJson);
//парсим json
$ArrayBookJson = json_decode($dataJson);
//Проверяем на ошибки
switch (json_last_error()) {
    case JSON_ERROR_NONE:
        echo ' - Ошибок нет';
        break;
    case JSON_ERROR_DEPTH:
        echo ' - Достигнута максимальная глубина стека';
        break;
    case JSON_ERROR_STATE_MISMATCH:
        echo ' - Некорректные разряды или несоответствие режимов';
        break;
    case JSON_ERROR_CTRL_CHAR:
        echo ' - Некорректный управляющий символ';
        break;
    case JSON_ERROR_SYNTAX:
        echo ' - Синтаксическая ошибка, некорректный JSON';
        break;
    case JSON_ERROR_UTF8:
        echo ' - Некорректные символы UTF-8, возможно неверно закодирован';
        break;
    default:
        echo ' - Неизвестная ошибка';
        break;
}

//переходим на уровень items
$k = $ArrayBookJson->items;

for ($i = 0; $i < count($k);$i++)
{
    $z = ($k[$i]);
    $volumeInfo = $z->volumeInfo;
    $title = $volumeInfo->title;
    $title = str_replace(',','',$title);
    if (!empty($volumeInfo->authors)){$authors = $volumeInfo->authors;} else{$authors=[''];};

    file_put_contents('./books.csv',$title.','.$authors[0]."\n", FILE_APPEND);

}


